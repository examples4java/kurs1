/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interfaces;
/**
 *
 * @author tez
 */
public interface Calculation {

    /**
     *
     * @param a
     * @param b
     * @return
     */
    public int add(int a, int b);

    /**
     *
     * @param a
     * @return
     */
    public long sub(long a[]);
    
}
