/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AbstractClass;

/**
 *
 * @author tez
 */
public abstract class Perimeters implements Interfaces.Calculation{

    public abstract void rectangle(int a, int b);
    
    @Override
    public int add(int a, int b) {
        return 2*a + 2*b;
    }

    @Override
    public long sub(long[] a) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
