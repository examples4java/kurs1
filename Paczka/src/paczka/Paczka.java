/*
    
 */
package paczka;

//poniższy import jest niepotrzebny - 
import Interfaces.*;
import AbstractClass.*;
//import com.sun.corba.se.impl.ior.ByteBuffer;
//import com.sun.xml.internal.stream.util.BufferAllocator;
//import com.sun.xml.internal.ws.commons.xmlutil.Converter;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author tez
 */
class MyPerimiters extends Perimeters {
    ///feefefefef
    @Override
    public void rectangle(int a, int b) {
        System.out.print("Obwód podanego prostokąta to: ");
        System.out.println(add(a, b));
    }
    
}

class testingPriv {
    protected void testowa(int a) {
        System.out.print("Lazy day...");
    }
    
    public void pisz() {testowa(0);}
}

public class Paczka extends Trigonometry implements Interfaces.Calculation, Calculation {

    private static final int ARRAYSIZE = Long.BYTES * Long.SIZE; 
    
    @Override
    public final float cos(int b, int c) {
        float ret = b/c;
        return ret;
    } 
    
    
    
    public float licz() {
        return super.sin(3, 5);
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        byte dataIn[] = new byte[Long.BYTES*Long.SIZE];
        
//        Perimeters perimiters = new MyPerimiters();        
//        System.out.println(perimiters.add(10, 3));
        
        System.out.println("Witaj w Java!");
        System.out.println(new Paczka().add(19, 5));
        Paczka p = new Paczka();
        //System.out.println(new Trigonometry().cos(3,5));
        testingPriv tp = new testingPriv() {
            @Override
            protected void testowa(int a) {
                System.out.println("Modyfikacja!");
            }
        };
        tp.pisz();
        System.out.println(p.licz());
        System.out.println(p.cos(3,5));
        
        new MyPerimiters().rectangle(2, 3);
        
        System.out.println(p.factorial(3));
        try { 
            
            System.in.read(dataIn);
            ByteBuffer bb = ByteBuffer.allocate(Long.BYTES*Long.SIZE);
            bb.put(dataIn);
            bb.flip();
            String ss = byteToString(dataIn);
            List<String> sList;
            sList = Arrays.asList(ss.split(" "));
            sList.set(sList.size()-1, sList.get(sList.size()-1).replaceAll("[^\\d-]", ""));
            System.out.println(new Paczka().sub(stringToLong(sList)));
        } catch (IOException ex) {
            Logger.getLogger(Paczka.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    private static String byteToString(byte[] b) {
        String ret = "";
        for (int i = 0; i < b.length; i++) 
            ret += (char)b[i];
        return ret;
    }
    
    private static long[] stringToLong(List<String> ls) {
        long[] ret = new long[ls.size()];
        
        Consumer<String> lArray = new Consumer<String>() {
            int i = 0;
            @Override
            public void accept(String t) {
                i = ls.indexOf(t);
                ret[i] = Long.parseLong(t);
            }
        };
        //ls.forEach(lArray);
        ls.forEach((String ss)->{
            int i = ls.indexOf(ss);
            ret[i] = Long.parseLong(ss);
        });
        return ret;
    }
    
    private static long[] convertBytes(byte[] a) {
        
        long ret[] = new long[a.length];
        for (int i = 0; i < a.length; i++) {
            ret[i] = Byte.toUnsignedLong(a[i]);
        }
        return ret;
    }

    @Override
    public int add(int a, int b) {
        return a+b;
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public long sub(long[] a) {
        if (a.length % 2 != 0) throw new UnsupportedOperationException("Nie można odejmować nieparzystej ilości liczb!");
        long ret = 0;
        int i = 0;
        while (a.length>i) {
            ret += a[i++];
            ret -= a[i++];
            //Object next = .next();
        }       
        return ret;       
    }

    @Override
    public long factorial(long a) {
        if (a > 0)
            return a*factorial(--a);
        return 1;
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }   

    @Override
    protected float cot(int b, int a) {
        return b/a * 1.f;
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }


}
